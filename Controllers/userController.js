const PatientDetails = require('../models/userSchema');

module.exports = {
  PostPatient: (req, res, next) => {
    const file = {
      user_id: req.body.user_id,
      data: req.body.data
    }
    console.log(file)
    const data = new PatientDetails({
      user_id: req.body.user_id,
      data: req.body.data
    })
    console.log(data)
    data.save().then((user) => {
      res.status(200).json({
        status: "Success",
        status_code: res.statusCode,
        data: user
      })
    }).catch((err) => {
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        error: err
      })
    })
  },
  getPatient: (req, res, next) => {
    PatientDetails.find({ user_id: req.params.id }).then((data) => {
      // console.log(data)
      const file = [];
      data.forEach((res, i) => {
        file.push(res.data[0])
      })
      // console.log(file)
      res.status(200).json({
        status: "Success",
        status_code: res.statusCode,
        data: file
      })
    }).catch((err) => {
      console.log(err)
      res.status(400).json({
        status: "Failed",
        status_code: res.statusCode,
        data: {
          error: err
        }
      })
    })
  }
}