const mongoose = require('mongoose');


const UserSchema = new mongoose.Schema({
  user_id: {
    type: String
  },
  data: {
    type: Array
  }
}, {
  timestamps: Date
})

module.exports = mongoose.model('PatientDetails', UserSchema)