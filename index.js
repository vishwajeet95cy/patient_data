const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config()
const userRoutes = require('./routes/userRoutes')

mongoose.connect(process.env.DB_HOST, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }, (err, db) => {
  if (err) throw err;
  console.log('Database Connected')
});

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

app.get('/', (req, res, next) => {
  res.send('Welcome to Express Patient')
})

app.use('/user', userRoutes)

app.listen(process.env.PORT, (req, res, next) => {
  console.log(`Server is connected to http://localhost:${process.env.PORT}`)
})
